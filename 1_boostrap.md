# Input

* Do the AWS Kafka Service
* It must be best!
* Money is not a problem.
* It must be delivered fast - time is the problem.

It should provide integration between systemsL CRM, ERP and others.


Provide Technical Design of integration platform.

## Questions

* How are we going to measure it is the best? What criterias are important?

    Proposal:

    * Latency
    * Message Throughput

* What is current update rate for each component?
* How many components that needs to be synchronized do we have currently?

* Is there a test system to play with existing components?
* 


## Solution draft

Publisher subscriber flow:

```mermaid
graph TD
    S1[System1] -->|publish update| IT1(input-topic1)
        IT1-->I[Kafka Connect Transformation]
            I-->OT1(output-topic1)
                I-->OT2(output-topic2)
                    I-->OT3(output-topic3)
                        OT1 -->|Listen for new messages| C2[System2]
                            OT2 -->|Listen for new messages| C3[System3]
                                OT3 -->|Listen for new messages| Cn[SystemN]
                                    Cn -->|Cn Offset commmit| OT3

                                        IT1-->KC[Kafka Connect]
                                            KC-->DB(Historical DB)
```

Actors:
* *System 1-n*: Produces and consumers of the data
* *Historical DB*: stores events in persistant database

In case of need to re-run updates in the system

```mermaid
graph TD
    ADM(Admin) --> |Force Update| ER(Event Restore)
    ER --> |Load Data| H(Historical DB)
    ER --> |publish historical events| T1(input-topic1)
```


Constraints:
* ˜˜There is a single source of truth˜˜
* Update events have limmited TTL
* Update events can be safely pushed twice: system is immune to duplicates
* Listeners must not republish original change


The responsibility of the client is to publish and handle changes in their native format (to input-topic and from output-topic)

### Delivery

TBD

### Monitoring

Main metrics:

* Queue size (offsets on all topics)
* Input-output data latency
* Sessions per topic

TBD

### Cost analysis

TBD